import Vue from 'vue'
import App from './App'
import uView from 'uview-ui';

Vue.config.productionTip = false
Vue.use(uView);
App.mpType = 'app'

Vue.prototype.$url = 'http://192.168.50.181:8888'


const app = new Vue({
    ...App
})
app.$mount()
